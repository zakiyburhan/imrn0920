// soal nomor 1
console.log("LOOPING PERTAMA");
angka=2;
while (angka<=20){
    console.log(angka+"-"+" I love coding");
    angka+=2;
}
console.log("----------------------");
angka2=20;
while(angka2>0){
    console.log(angka2+"-"+" I'll be a mobile developer InsyaAllah");
    angka2-=2;
}
    
console.log("-----------------\n");
// soal nomor 2 {looping menggunakan for}
for(angkafor=1;angkafor<=20;angkafor++){
    if((angkafor%3==0)&&(angkafor%2==1)){
        console.log(+angkafor+"- I love coding")
    }else if(angkafor%2==0){
        console.log(angkafor+"- Berkualitas");
    }else if(angkafor%2==1){
        console.log(angkafor+"- Santai");

    }
}
// soal nomor 3 
for(kotak=1;kotak<=4;kotak++){
    console.log("########");
}
console.log("---------------\n");
// soal nomor 4
tagar="#";
looping=1;
while(looping<=7){
    console.log(tagar);
    tagar=tagar+"#";
    looping++;
}
//soal nomor 5
console.log("------------\n");
var size=8;
var board="";
for(x=0;x<size;x++){
    for(y=0;y<size;y++){
        if((x+y)%2==0){
            board+=" ";
          
        }else board+="#";  

    }board+="\n";
}console.log(board);

