// soal nomor1 mengubah fungsi menjadi fungsi arrow

/*const golden = function goldenFunction(){
    console.log("this is golden!!")
  }
   
  golden()*/
  const golden=goldenFunction=()=>{
      console.log("this is golden!")
  }
  golden()

console.log("_________________________________\n")
//   soal nomor2 sederhanakan menjadi object literal
const newFunction = function literal(firstName, lastName){
  return {
    firstName,
    lastName,
    fullName() {
      console.log(`${firstName} ${lastName}`)
      return 
    }
  };
}
 
//Driver Code 
newFunction("William", "Imoh").fullName()
// soal nomor3
console.log("____________________________\n")
const newObject = {
  firstName: "Harry",
  lastName: "Potter Holt",
  destination: "Hogwarts React Conf",
  occupation: "Deve-wizard Avocado",
  spell: "Vimulus Renderus!!!"
}
const{firstName,lastName,destination,occupation,spell}=newObject
console.log(firstName, lastName, destination, occupation)
// soal nomor 4
console.log("______________________")
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west,...east]
//Driver Code
console.log(combined)
// soal nomor 5
console.log("_______________________\n")
const planet = "earth"
const view = "glass"
var before = 'Lorem ' + view + 'dolor sit amet, ' +  
    'consectetur adipiscing elit,' + planet + 'do eiusmod tempor ' +
    'incididunt ut labore et dolore magna aliqua. Ut enim' +
    ' ad minim veniam'
 const newbefore=`${planet} ${view} ${before}`
// Driver Code
console.log(newbefore) 